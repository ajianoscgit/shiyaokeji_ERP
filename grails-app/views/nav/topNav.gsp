<%--
  Created by IntelliJ IDEA.
  User: Ajian
  Date: 14-3-6
  Time: 下午2:19
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="constant.SystemConstant" contentType="text/html;charset=UTF-8" %>
<!--顶部导航栏-->
<div class="span12">
    %{--<h3 class="page-title">
        ${session.getAttribute(SystemConstant.UserNowInHeadMenuKEY)} <small>${session.getAttribute(SystemConstant.UserNowInChildMenuKEY)}</small>
    </h3>--}%
    <br>
    <ul class="breadcrumb">
        <li>
            <i class="${session.getAttribute(SystemConstant.UserNowInHeadMenuIcoKEY)}"></i>
            <a href="javascript:void(0);">${session.getAttribute(SystemConstant.UserNowInHeadMenuKEY)}</a>
            <g:if test="${session.getAttribute(SystemConstant.UserNowInChildMenuKEY)}">
                <i class="icon-angle-right"></i>
            </g:if>
        </li>
        <li><a href="javascript:void(0);">${session.getAttribute(SystemConstant.UserNowInChildMenuKEY)}</a></li>
        <li class="pull-right no-text-shadow">
            <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                <i class="icon-calendar"></i>
                <span></span>
                <i class="icon-angle-down"></i>
            </div>
        </li>
    </ul>
</div>