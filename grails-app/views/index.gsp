<%--
  Created by IntelliJ IDEA.
  User: Ajian
  Date: 14-3-5
  Time: 下午2:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<head>
    <meta name="layout" content="admin_layout"/>
    <title>系统首页</title>
</head>

<body>
<div id="dashboard">
    <div class="row-fluid">
        <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="icon-comments"></i>
                </div>
                <div class="details">
                    <div class="desc">本月进项额:</div>
                    <div class="number">${inMoney?:0}元</div>
                </div>
                <% Calendar c = Calendar.getInstance()
                    c.set(Calendar.DAY_OF_MONTH,1)
                    %>
                <a class="more" href="<g:createLink controller="invoice" action="indexByWhere" params="['kind':1,'m':new java.text.SimpleDateFormat('yyyy-MM-dd').format(c.getTime())]" /> ">
                    本月进项 <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>

        <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="icon-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="desc">本月销项额:</div>
                    <div class="number">${outMoney?:0}元</div>
                </div>
                <a class="more" href="<g:createLink controller="invoice" action="indexByWhere" params="['kind':2,'m':new java.text.SimpleDateFormat('yyyy-MM-dd').format(c.getTime())]" /> ">
                    本月销项 <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>

        <div class="span3 responsive" data-tablet="span6  fix-offset" data-desktop="span3">
            <div class="dashboard-stat purple">
                <div class="visual">
                    <i class="icon-globe"></i>
                </div>
                <div class="details">
                    <div class="desc">本月进销余额:</div>
                    <div class="desc">${inMoney}-${outMoney}=</div>
                    <div class="number">${Float.parseFloat((inMoney-outMoney).toString())}元</div>
                </div>
                <a class="more" style="cursor:auto;;" href="javascript:void(0);">
                    %{--本月进销余额 <i class="m-icon-swapright m-icon-white"></i>--}%
                    &nbsp;
                </a>
            </div>
        </div>

        <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
            <div class="dashboard-stat yellow">
                <div class="visual">
                    <i class="icon-bar-chart"></i>
                </div>
                <div class="details">
                    <div class="desc">总进销余额:</div>
                    <div class="number">${Float.parseFloat(allMoney.toString())?:0}元</div>
                </div>
                <a class="more" style="cursor:auto;;" href="javascript:void(0);">
                    %{--总进销余额 <i class="m-icon-swapright m-icon-white"></i>--}%
                    &nbsp;
                </a>
            </div>
        </div>
    </div>

    <hr>
    <h1>货物库存</h1>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>${message(code: 'product.name.label', default: 'Name')}</th>
            <th>商品库存</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${productKucun.entrySet()}" status="i" var="pk">
            <tr class="${pk.getValue() == 0 ? 'bg-red' : 'odd'}">
                <td>${pk.getKey().name}</td>
                <td>${pk.getValue()}</td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <hr>

    <h1>最近发票</h1>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th><g:message code="invoice.invoiceType.label" default="Invoice Type" /></th>
            <th>${message(code: 'invoice.invoiceNumber.label',default: '发票编号')}" </th>
            <th><g:message code="invoice.supplier.label" default="购货单位" /></th>
            <th><g:message code="invoice.seller.label" default="销货单位" /></th>
            <th><g:message code="invoice.product.label" default="货物" /></th>
            <th><g:message code="invoice.xinghao.label" default="规格型号" /></th>
            <th><g:message code="invoice.danwei.label" default="单位" /></th>
            <th><g:message code="invoice.count.label" default="数量" /></th>
            <th><g:message code="invoice.price.label" default="单价" /></th>
            <th><g:message code="invoice.totalMoney.label" default="金额" /></th>
            <th><g:message code="invoice.taxRate.label" default="税率" /></th>
            <th><g:message code="invoice.tax.label" default="税额" /></th>
            <th>${message(code: 'invoice.dateCreated.label', default: '开票日期')}</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${invoiceInstanceList}" status="i" var="invoiceInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                <td>${fieldValue(bean: invoiceInstance, field: "invoiceType.name")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "invoiceNumber")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "buyCompany.name")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "sellCompany.name")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "product.name")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "xinghao")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "danwei")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "count")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "price")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "totalMoney")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "taxRate")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "tax")}</td>
                <td><g:formatDate date="${invoiceInstance.dateCreated}" format="yyyy年MM月dd日" /></td>
            </tr>
        </g:each>
        </tbody>
    </table>

</div>
</body>
</html>