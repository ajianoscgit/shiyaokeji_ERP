
<%@ page import="shiyaokeji_erp.InvoiceType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin_layout">
		<g:set var="entityName" value="${message(code: 'invoiceType.label', default: 'InvoiceType')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-invoiceType" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-invoiceType" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list invoiceType">
			
				<g:if test="${invoiceTypeInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="invoiceType.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${invoiceTypeInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${invoiceTypeInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="invoiceType.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${invoiceTypeInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${invoiceTypeInstance?.invoices}">
				<li class="fieldcontain">
					<span id="invoices-label" class="property-label">包含发票的创建时间</span>
					
						<g:each in="${invoiceTypeInstance.invoices}" var="i">
						<span class="property-value" aria-labelledby="invoices-label"><g:link controller="invoice" action="show" id="${i.id}">${i?.dateCreated.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>

			</ol>
			<g:form url="[resource:invoiceTypeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${invoiceTypeInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
