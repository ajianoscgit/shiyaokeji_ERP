
<%@ page import="shiyaokeji_erp.InvoiceType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin_layout">
		<g:set var="entityName" value="${message(code: 'invoiceType.label', default: 'InvoiceType')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-invoiceType" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-invoiceType" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-bordered">
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'invoiceType.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'invoiceType.dateCreated.label', default: 'Date Created')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${invoiceTypeInstanceList}" status="i" var="invoiceTypeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${invoiceTypeInstance.id}">${fieldValue(bean: invoiceTypeInstance, field: "name")}</g:link></td>
					
						<td><g:formatDate date="${invoiceTypeInstance.dateCreated}" /></td>
					

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${invoiceTypeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
