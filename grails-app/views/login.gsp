<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"><!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <title>系统登录</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>

    <link href="<g:resource dir="css" file="bootstrap.min.css" />" rel="stylesheet" type="text/css"/>
    <link href="<g:resource dir="css" file="bootstrap-responsive.min.css" />" rel="stylesheet" type="text/css"/>
    <link href="<g:resource dir="css" file="font-awesome.min.css" />" rel="stylesheet" type="text/css"/>
    <link href="<g:resource dir="css" file="style-metro.css" />" rel="stylesheet" type="text/css"/>
    <link href="<g:resource dir="css" file="style.css" />" rel="stylesheet" type="text/css"/>
    <link href="<g:resource dir="css" file="style-responsive.css" />" rel="stylesheet" type="text/css"/>
    <link href="<g:resource dir="css" file="default.css" />" rel="stylesheet" type="text/css"/>
    <link href="<g:resource dir="css" file="uniform.default.css" />" rel="stylesheet" type="text/css"/>

    <link href="<g:resource dir="css" file="login.css" />" rel="stylesheet" type="text/css"/>
</head>

<body class="login">
<div class="logo">&nbsp;</div>
<div class="content">
<form class="form-vertical login-form" action="<g:createLink controller="user" action="login" />" method="post">
    <h3 class="form-title">请在此输入你的账户信息</h3>
    <div class="alert alert-error <g:if test="${!flash.message}">hide</g:if>">
        <button class="close" data-dismiss="alert"></button>
        <span><g:if test="${!flash.message}">用户名或密码输入不完整,请检查!</g:if><g:else>${flash.message}</g:else></span>
    </div>

    <div class="control-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">用户名</label>
        <div class="controls">
            <div class="input-icon left">
                <i class="icon-user"></i>
                <input class="m-wrap placeholder-no-fix" type="text" placeholder="用户名" name="name"/>
            </div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">密码</label>
        <div class="controls">
            <div class="input-icon left">
                <i class="icon-lock"></i>
                <input class="m-wrap placeholder-no-fix" type="password" placeholder="密码" name="password"/>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <button type="submit" class="btn green pull-right">登录</button>
    </div>

</form>
</div>

<g:javascript src="jquery-1.10.1.min.js"/>
<g:javascript src="jquery-migrate-1.2.1.min.js"/>
<g:javascript src="jquery-ui-1.10.1.custom.min.js"/>
<g:javascript src="bootstrap.min.js"/>
<!--[if lt IE 9]>
    <g:javascript src="excanvas.min.js"/>
    <g:javascript src="respond.min.js"/>
	<![endif]-->
<g:javascript src="jquery.slimscroll.min.js"/>
<g:javascript src="jquery.blockui.min.js"/>
<g:javascript src="jquery.cookie.min.js"/>
<g:javascript src="jquery.uniform.min.js"/>
<g:javascript src="jquery.validate.min.js"/>

<g:javascript src="app.js"/>
<g:javascript src="login.js"/>

<script type="text/javascript">
    $(function () {
        App.init();
        Login.init();
    });

    var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-37564768-1']);
_gaq.push(['_setDomainName', 'keenthemes.com']);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_trackPageview']);
(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();
</script>
</body>
</html>