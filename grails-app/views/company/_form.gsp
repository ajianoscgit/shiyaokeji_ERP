<%@ page import="shiyaokeji_erp.Company" %>



<div class="fieldcontain ${hasErrors(bean: companyInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="company.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${companyInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: companyInstance, field: 'sui_number', 'error')} ">
	<label for="sui_number">
		<g:message code="company.sui_number.label" default="Suinumber" />
		
	</label>
	<g:textField name="sui_number" value="${companyInstance?.sui_number}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: companyInstance, field: 'phone', 'error')} ">
	<label for="phone">
		<g:message code="company.phone.label" default="Phone" />
		
	</label>
	<g:textField name="phone" value="${companyInstance?.phone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: companyInstance, field: 'bankAddress', 'error')} ">
	<label for="bankAddress">
		<g:message code="company.bankAddress.label" default="Bank Address" />
		
	</label>
	<g:textField name="bankAddress" value="${companyInstance?.bankAddress}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: companyInstance, field: 'bankNumber', 'error')} ">
    <label for="bankNumber">
        <g:message code="company.bankNumber.label" default="Bank Number" />

    </label>
    <g:textField name="bankNumber" value="${companyInstance?.bankNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: companyInstance, field: 'address', 'error')} ">
	<label for="address">
		<g:message code="company.address.label" default="Address" />
		
	</label>
	<g:textField name="address" value="${companyInstance?.address}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: companyInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="company.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${companyInstance?.description}"/>
</div>