
<%@ page import="shiyaokeji_erp.Company" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin_layout">
		<g:set var="entityName" value="${message(code: 'company.label', default: 'Company')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-company" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-company" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-bordered">
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'company.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="sui_number" title="${message(code: 'company.sui_number.label', default: 'Suinumber')}" />
					
						<g:sortableColumn property="phone" title="${message(code: 'company.phone.label', default: 'Phone')}" />
					
						<g:sortableColumn property="bankAddress" title="${message(code: 'company.bankAddress.label', default: 'Bank Address')}" />
					
						<g:sortableColumn property="address" title="${message(code: 'company.address.label', default: 'Address')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'company.description.label', default: 'Description')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${companyInstanceList}" status="i" var="companyInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${companyInstance.id}">${fieldValue(bean: companyInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: companyInstance, field: "sui_number")}</td>
					
						<td>${fieldValue(bean: companyInstance, field: "phone")}</td>
					
						<td>${fieldValue(bean: companyInstance, field: "bankAddress")}</td>
					
						<td>${fieldValue(bean: companyInstance, field: "address")}</td>
					
						<td>${fieldValue(bean: companyInstance, field: "description")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${companyInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
