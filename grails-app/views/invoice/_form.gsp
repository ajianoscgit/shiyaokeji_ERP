<%@ page import="shiyaokeji_erp.Invoice" %>
<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceType', 'error')} required">
    <label for="invoiceType">
        <g:message code="invoice.invoiceType.label" default="Invoice Type" />
        <span class="required-indicator">*</span>
    </label>
    <g:select id="invoiceType" name="invoiceType.id" from="${shiyaokeji_erp.InvoiceType.list()}" optionKey="id" optionValue="name" required="" value="${invoiceInstance?.invoiceType?.id?:1}" class="many-to-one"/>
</div>
<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceNumber', 'error')} ">
    <label for="invoiceNumber">
        <g:message code="invoice.invoiceNumber.label" default="Invoice Number" />

    </label>
    <g:textField name="invoiceNumber" value="${invoiceInstance?.invoiceNumber}"/>
</div>
<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'buyCompany', 'error')} required">
    <label for="supplier">
        <g:message code="invoice.supplier.label" default="Supplier" />
        <span class="required-indicator">*</span>
    </label>
    <g:select id="supplier" name="buyCompany.id" from="${shiyaokeji_erp.Company.list()}" optionKey="id" optionValue="name" required="" value="${invoiceInstance?.buyCompany?.id?:1}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'sellCompany', 'error')} required">
    <label for="seller">
        <g:message code="invoice.seller.label" default="Seller" />
        <span class="required-indicator">*</span>
    </label>
    <g:select id="seller" name="sellCompany.id" from="${shiyaokeji_erp.Company.list()}" optionKey="id" optionValue="name" required="" value="${invoiceInstance?.sellCompany?.id?:1}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'product', 'error')} required">
    <label for="product">
        <g:message code="invoice.product.label" default="Product" />
        <span class="required-indicator">*</span>
    </label>
    <g:select id="product" name="product.id" from="${shiyaokeji_erp.Product.list()}" optionKey="id" optionValue="name" required="" value="${invoiceInstance?.product?.id?:1}"  class="many-to-one"/>
</div>
<div class="row-fluid" style="padding-left: 10%;">
<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'xinghao', 'error')} span3 text-right" >
	<label for="xinghao">
		<g:message code="invoice.xinghao.label" default="Xinghao" />
	</label>
	<g:textField name="xinghao" value="${invoiceInstance?.xinghao}" style="width:40px;"/>
</div>
<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'danwei', 'error')} span3 text-left" >
    <label for="danwei">
        <g:message code="invoice.danwei.label" default="Danwei" />
    </label>
    <g:textField name="danwei" value="${invoiceInstance?.danwei}" style="width:40px;"/>
</div>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'count', 'error')} required">
	<label for="count">
		<g:message code="invoice.count.label" default="Count" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="count" type="number" value="${invoiceInstance.count}" required="number"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'price', 'error')} required">
	<label for="price">
		<g:message code="invoice.price.label" default="Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="price" value="${fieldValue(bean: invoiceInstance, field: 'price')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'totalMoney', 'error')} required">
    <label for="totalMoney">
        <g:message code="invoice.totalMoney.label" default="Total Money" />
        <span class="required-indicator">*</span>
    </label>
    <g:field name="totalMoney" value="${fieldValue(bean: invoiceInstance, field: 'totalMoney')}" required="" style="readOnly:readOnly;" readOnly="readonly" />
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'taxRate', 'error')} required">
	<label for="taxRate">
		<g:message code="invoice.taxRate.label" default="Tax Rate" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="taxRate" value="${fieldValue(bean: invoiceInstance, field: 'taxRate')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'tax', 'error')} required">
    <label for="tax">
        <g:message code="invoice.tax.label" default="Tax" />
        <span class="required-indicator">*</span>
    </label>
    <g:field name="tax" value="${fieldValue(bean: invoiceInstance, field: 'tax')}" required="" style="readOnly:readOnly;" readOnly="readonly"/>
</div>
<r:script>
    $(function(){
        $("#price").val($("#price").val().replace(',',''));
        var count= 0,price= 0,total=0;
        var totalMoneyDOM = $("#totalMoney");
        $("#count,#price").blur(function(){
            count= $("#count").val();
            price=$("#price").val();
            total=parseInt(count)*parseFloat(price);
            if(!isNaN(count) && !isNaN(price) && !isNaN(total)){
                totalMoneyDOM.val(total.toFixed(2));
            }else{
                alert('单价或数量输入有误,请检查.')
                totalMoneyDOM.val('');
            }
        });
        var taxRate=0;
        $("#taxRate").blur(function(){
            if(totalMoneyDOM.val()==''){
                alert('单价或数量输入有误,请检查.');
                $("#tax").val('')
                return
            }
            if(this.value=='' || isNaN(this.value)){
                alert('税率输入有误,请检查.');
                $("#tax").val('')
                return;
            }
            $("#tax").val((parseFloat(this.value)*parseFloat(totalMoneyDOM.val())).toFixed(2))
        });
    });
</r:script>