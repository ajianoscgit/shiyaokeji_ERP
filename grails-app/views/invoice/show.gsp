<%@ page import="shiyaokeji_erp.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin_layout">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-invoice" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list invoice">
                <g:if test="${invoiceInstance?.invoiceType}">
                    <li class="fieldcontain">
                        <span id="invoiceType-label" class="property-label"><g:message code="invoice.invoiceType.label" default="Invoice Type" /></span>
                        <span class="property-value" aria-labelledby="invoiceType-label"><g:link controller="invoiceType" action="show" id="${invoiceInstance?.invoiceType?.id}">${invoiceInstance?.invoiceType?.name.encodeAsHTML()}</g:link></span>
                    </li>
                </g:if>
                <g:if test="${invoiceInstance?.invoiceNumber}">
                    <li class="fieldcontain">
                        <span id="invoiceNumber-label" class="property-label"><g:message code="invoice.invoiceNumber.label" default="Invoice Number" /></span>

                        <span class="property-value" aria-labelledby="invoiceNumber-label"><g:fieldValue bean="${invoiceInstance}" field="invoiceNumber"/></span>

                    </li>
                </g:if>
                <g:if test="${invoiceInstance?.buyCompany}">
                    <li class="fieldcontain">
                        <span id="supplier-label" class="property-label"><g:message code="invoice.supplier.label" default="Supplier" /></span>

                        <span class="property-value" aria-labelledby="supplier-label"><g:link controller="company" action="show" id="${invoiceInstance?.buyCompany?.id}">${invoiceInstance?.buyCompany?.name.encodeAsHTML()}</g:link></span>

                    </li>
                </g:if>
                <g:if test="${invoiceInstance?.sellCompany}">
                    <li class="fieldcontain">
                        <span id="seller-label" class="property-label"><g:message code="invoice.seller.label" default="Seller" /></span>

                        <span class="property-value" aria-labelledby="seller-label"><g:link controller="company" action="show" id="${invoiceInstance?.sellCompany?.id}">${invoiceInstance?.sellCompany?.name.encodeAsHTML()}</g:link></span>

                    </li>
                </g:if>
                <g:if test="${invoiceInstance?.product}">
                    <li class="fieldcontain">
                        <span id="product-label" class="property-label"><g:message code="invoice.product.label" default="Product" /></span>

                        <span class="property-value" aria-labelledby="product-label"><g:link controller="product" action="show" id="${invoiceInstance?.product?.id}">${invoiceInstance?.product?.name.encodeAsHTML()}</g:link></span>

                    </li>
                </g:if>

                <g:if test="${invoiceInstance?.xinghao}">
				<li class="fieldcontain">
					<span id="xinghao-label" class="property-label"><g:message code="invoice.xinghao.label" default="Xinghao" /></span>
						<span class="property-value" aria-labelledby="xinghao-label"><g:fieldValue bean="${invoiceInstance}" field="xinghao"/></span>
				</li>
				</g:if>
			
				<g:if test="${invoiceInstance?.danwei}">
				<li class="fieldcontain">
					<span id="danwei-label" class="property-label"><g:message code="invoice.danwei.label" default="Danwei" /></span>
						<span class="property-value" aria-labelledby="danwei-label"><g:fieldValue bean="${invoiceInstance}" field="danwei"/></span>
				</li>
				</g:if>
			
				<g:if test="${invoiceInstance?.count}">
				<li class="fieldcontain">
					<span id="count-label" class="property-label"><g:message code="invoice.count.label" default="Count" /></span>
					
						<span class="property-value" aria-labelledby="count-label"><g:fieldValue bean="${invoiceInstance}" field="count"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${invoiceInstance?.price}">
				<li class="fieldcontain">
					<span id="price-label" class="property-label"><g:message code="invoice.price.label" default="Price" /></span>
					
						<span class="property-value" aria-labelledby="price-label"><g:fieldValue bean="${invoiceInstance}" field="price"/></span>
					
				</li>
				</g:if>
                <g:if test="${invoiceInstance?.totalMoney}">
                    <li class="fieldcontain">
                        <span id="totalMoney-label" class="property-label"><g:message code="invoice.totalMoney.label" default="Total Money" /></span>

                        <span class="property-value" aria-labelledby="totalMoney-label"><g:fieldValue bean="${invoiceInstance}" field="totalMoney"/></span>

                    </li>
                </g:if>
                <g:if test="${invoiceInstance?.taxRate}">
                    <li class="fieldcontain">
                        <span id="taxRate-label" class="property-label"><g:message code="invoice.taxRate.label" default="Tax Rate" /></span>

                        <span class="property-value" aria-labelledby="taxRate-label"><g:fieldValue bean="${invoiceInstance}" field="taxRate"/></span>

                    </li>
                </g:if>
				<g:if test="${invoiceInstance?.tax}">
				<li class="fieldcontain">
					<span id="tax-label" class="property-label"><g:message code="invoice.tax.label" default="Tax" /></span>
					
						<span class="property-value" aria-labelledby="tax-label"><g:fieldValue bean="${invoiceInstance}" field="tax"/></span>
					
				</li>
				</g:if>
                <g:if test="${invoiceInstance?.dateCreated}">
                    <li class="fieldcontain">
                        <span id="dateCreated-label" class="property-label"><g:message code="invoice.dateCreated.label" default="Date Created" /></span>

                        <span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${invoiceInstance?.dateCreated}" format="yyyy年MM月dd日" /></span>

                    </li>
                </g:if>
			
			</ol>
			<g:form url="[resource:invoiceInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${invoiceInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
