
<%@ page import="shiyaokeji_erp.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin_layout">
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <g:javascript src="My97DatePicker/WdatePicker.js" />
    <r:script>
        $(function(){
            $("#startB").WdatePicker();
        });
    </r:script>
</head>
<body>
<a href="#list-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="list-invoice" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" />
    <g:if test="${kind==1}">
        &nbsp;<i class="icon-angle-right"></i>&nbsp;进项列表
    </g:if><g:elseif test="${kind==2}">
        &nbsp;<i class="icon-angle-right"></i>&nbsp;销项列表
    </g:elseif>
    </h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div>
        <form name="searchByInvoiceKind" method="get" action="<g:createLink controller="invoice" action="indexByWhere" />">
            查看类别：<g:select name="kind" from="${[['v':'进项列表','k':1],['v':'销项列表','k':2]]}" optionKey="k" optionValue="v" value="${kind?:1}" style="width:120px;" />
            开始时间：<input type="text" id="startB" name="m" onClick="WdatePicker()" readonly="readonly" value="${m?:new java.text.SimpleDateFormat('yyyy-MM-dd').format(new Date())}">
            结束时间：<input type="text" id="startE" name="e" onClick="WdatePicker()" readonly="readonly" value="${e?:new java.text.SimpleDateFormat('yyyy-MM-dd').format(new Date())}">
            <input type="submit" value="搜索" class="button" />
        </form>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th><g:message code="invoice.invoiceType.label" default="Invoice Type" /></th>
            <g:sortableColumn property="invoiceNumber" title="${message(code: 'invoice.invoiceNumber.label',default: '发票编号')}" />
            <th><g:message code="invoice.supplier.label" default="购货单位" /></th>
            <th><g:message code="invoice.seller.label" default="销货单位" /></th>
            <th><g:message code="invoice.product.label" default="货物" /></th>
            <th><g:message code="invoice.xinghao.label" default="规格型号" /></th>
            <th><g:message code="invoice.danwei.label" default="单位" /></th>
            <th><g:message code="invoice.count.label" default="数量" /></th>
            <th><g:message code="invoice.price.label" default="单价" /></th>
            <th><g:message code="invoice.totalMoney.label" default="金额" /></th>
            <th><g:message code="invoice.taxRate.label" default="税率" /></th>
            <th><g:message code="invoice.tax.label" default="税额" /></th>
            <g:sortableColumn property="dateCreated" title="${message(code: 'invoice.dateCreated.label', default: '开票日期')}" />
        </tr>
        </thead>
        <tbody>
        <g:each in="${invoiceInstanceList}" status="i" var="invoiceInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                <td><a href="<g:createLink controller="invoice" action="show" id="${invoiceInstance.id}" />">${fieldValue(bean: invoiceInstance, field: "invoiceType.name")}</a></td>
                <td>${fieldValue(bean: invoiceInstance, field: "invoiceNumber")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "buyCompany.name")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "sellCompany.name")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "product.name")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "xinghao")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "danwei")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "count")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "price")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "totalMoney")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "taxRate")}</td>
                <td>${fieldValue(bean: invoiceInstance, field: "tax")}</td>
                <td><g:formatDate date="${invoiceInstance.dateCreated}" format="yyyy年MM月dd日" /></td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${invoiceInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>
