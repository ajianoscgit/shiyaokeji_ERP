<%@ page import="constant.SystemConstant" %>
<%
    if(!session.getAttribute(SystemConstant.USER_Session_KEY)){
        flash.message='登录失效，请执行登录！'
        response.sendRedirect("${request.getContextPath()}")
        return
    }
%>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="系统"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<g:resource dir="css" file="bootstrap.min.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="bootstrap-responsive.min.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="font-awesome.min.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="style-metro.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="style.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="style-responsive.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="default.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="uniform.default.css" />" rel="stylesheet" type="text/css"/>

        <link href="<g:resource dir="css" file="jquery.gritter.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="daterangepicker.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="fullcalendar.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="jqvmap.css" />" rel="stylesheet" type="text/css"/>
        <link href="<g:resource dir="css" file="jquery.easy-pie-chart.css" />" rel="stylesheet" type="text/css"/>

        <link href="${resource(dir: 'css', file: 'main.css')}"  rel="stylesheet" type="text/css">
        <link href="${resource(dir: 'css', file: 'mobile.css')}" rel="stylesheet"  type="text/css">

        <g:layoutHead/>
		<r:layoutResources />
	</head>
<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
    <div class="header navbar navbar-inverse navbar-fixed-top">
        <!-- BEGIN TOP NAVIGATION BAR -->
        <div class="navbar-inner">
            <div class="container-fluid" style="height: 40px;width:75%;float:left;">
                <div style="color: #ffffff;font-size: 2.5em;margin-top: 10px;">${constant.SystemConstant.SystemName}</div>
                <!-- BEGIN LOGO -->
                <a class="brand" href="javascript:void(0);">
                %{--<img src="${resource(dir: 'images',file: 'logo.png')}" alt="logo"/>--}%
                </a>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                    %{--<img src="${resource(dir: 'images',file: 'menu-toggler.png')}" alt="" />--}%
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
            </div>
            <div style="height:40px;width:20%;vertical-align: bottom;display: table-cell;color: white;">
                <%=session.getAttribute(SystemConstant.USER_Session_KEY).name %>，欢迎使用.
                &nbsp;&nbsp;&nbsp;
                <g:formatDate date="${new java.util.Date()}" format="yyyy年MM月dd日" />
                &nbsp;&nbsp;&nbsp;
                <a href="<g:createLink controller="user" action="loginOut" />">退出</a>
            </div>
        </div>
    </div>
    <!-- END HEADER -->

    <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar nav-collapse collapse">
        <!-- 侧边菜单 -->
        <ul class="page-sidebar-menu">
            <li>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone" title="收起/展开侧边菜单"></div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <li><div style="height:38px;">&nbsp;</div></li>
            <g:include controller="menu" action="indexMenu" />
        </ul>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div class="page-content">

    <!-- 提示框-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Widget Settings</h3>
        </div>
        <div class="modal-body">
            Widget settings form goes here
        </div>
    </div>

    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">

    <!-- 导航条-->
    <div class="row-fluid">
        <g:include view="/nav/topNav.gsp" />
    </div>

    <g:layoutBody/>
    </div>
    </div>
    </div>



    <!-- BEGIN FOOTER -->
    <div class="footer">
        <div class="footer-inner text-center" style="width:100%;">
            2014 &copy; Ajian Test.
        </div>
        <div class="footer-tools">
            <span class="go-top">
                <i class="icon-angle-up"></i>
            </span>
        </div>
    </div>
    <!-- END FOOTER -->

    <div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>

    <g:javascript src="jquery-1.10.1.min.js"/>
    <g:javascript src="jquery-migrate-1.2.1.min.js"/>
    <g:javascript src="jquery-ui-1.10.1.custom.min.js"/>
    <g:javascript src="bootstrap.min.js"/>
    <!--[if lt IE 9]>
    <g:javascript src="excanvas.min.js"/>
    <g:javascript src="respond.min.js"/>
    <![endif]-->
    <g:javascript src="jquery.slimscroll.min.js"/>
    <g:javascript src="jquery.blockui.min.js"/>
    <g:javascript src="jquery.cookie.min.js"/>
    <g:javascript src="jquery.uniform.min.js"/>
    <g:javascript src="jquery.validate.min.js"/>
    <g:javascript src="app.js"/>
    <g:javascript src="jquery.flot.js"/>
    <g:javascript src="jquery.flot.resize.js"/>
    <g:javascript src="jquery.pulsate.min.js"/>
    <g:javascript src="date.js"/>
    <g:javascript src="daterangepicker.js"/>
    <g:javascript src="jquery.gritter.js"/>
    <g:javascript src="fullcalendar.min.js"/>
    <g:javascript src="jquery.easy-pie-chart.js"/>
    <g:javascript src="jquery.sparkline.min.js"/>
    %{--<g:javascript src="index.js"/>--}%

    <script type="text/javascript">
        $(function () {
            App.init(); // initlayout and core plugins
            /*Index.init();
            Index.initJQVMAP(); // init index page's custom scripts
            Index.initCalendar(); // init index page's custom scripts
            Index.initCharts(); // init index page's custom scripts
            Index.initChat();
            Index.initMiniCharts();
            Index.initDashboardDaterange();
            Index.initIntro();*/
        });
    </script>
	<r:layoutResources />
</body>
</html>