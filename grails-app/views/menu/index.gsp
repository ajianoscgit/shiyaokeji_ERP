
<%@ page import="shiyaokeji_erp.Menu" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'menu.label', default: 'Menu')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-menu" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-menu" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-bordered">
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'menu.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="ico" title="${message(code: 'menu.ico.label', default: 'Ico')}" />
					
						<g:sortableColumn property="rank" title="${message(code: 'menu.rank.label', default: 'Rank')}" />
					
						<g:sortableColumn property="parentMenu" title="${message(code: 'menu.parentMenu.label', default: 'Parent Menu')}" />
					
						<g:sortableColumn property="link" title="${message(code: 'menu.link.label', default: 'Link')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${menuInstanceList}" status="i" var="menuInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${menuInstance.id}">${fieldValue(bean: menuInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: menuInstance, field: "ico")}</td>
					
						<td>${fieldValue(bean: menuInstance, field: "rank")}</td>
					
						<td>${fieldValue(bean: menuInstance, field: "parentMenu")}</td>
					
						<td>${fieldValue(bean: menuInstance, field: "link")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${menuInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
