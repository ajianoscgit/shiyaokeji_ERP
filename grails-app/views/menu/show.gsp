
<%@ page import="shiyaokeji_erp.Menu" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'menu.label', default: 'Menu')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-menu" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list menu">
			
				<g:if test="${menuInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="menu.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${menuInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${menuInstance?.ico}">
				<li class="fieldcontain">
					<span id="ico-label" class="property-label"><g:message code="menu.ico.label" default="Ico" /></span>
					
						<span class="property-value" aria-labelledby="ico-label"><g:fieldValue bean="${menuInstance}" field="ico"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${menuInstance?.rank}">
				<li class="fieldcontain">
					<span id="rank-label" class="property-label"><g:message code="menu.rank.label" default="Rank" /></span>
					
						<span class="property-value" aria-labelledby="rank-label"><g:fieldValue bean="${menuInstance}" field="rank"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${menuInstance?.parentMenu}">
				<li class="fieldcontain">
					<span id="parentMenu-label" class="property-label"><g:message code="menu.parentMenu.label" default="Parent Menu" /></span>
					
						<span class="property-value" aria-labelledby="parentMenu-label"><g:fieldValue bean="${menuInstance}" field="parentMenu"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${menuInstance?.link}">
				<li class="fieldcontain">
					<span id="link-label" class="property-label"><g:message code="menu.link.label" default="Link" /></span>
					
						<span class="property-value" aria-labelledby="link-label"><g:fieldValue bean="${menuInstance}" field="link"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:menuInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${menuInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
