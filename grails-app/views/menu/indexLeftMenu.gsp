<%--
  Created by IntelliJ IDEA.
  User: Ajian
  Date: 14-3-6
  Time: 下午1:21
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="shiyaokeji_erp.Menu;constant.SystemConstant" contentType="text/html;charset=UTF-8" %>
<g:each in="${menus}" var="menu" status="i">
    <li <g:if test="${session.getAttribute(SystemConstant.UserNowInHeadMenuKEY)==menu.name}">class="start active"</g:if>>
        <a href="${request.getContextPath()}${menu.link}">
            <i class="${menu.ico}"></i>
            <span class="title">${menu.name}</span>
            <g:if test="${session.getAttribute(SystemConstant.UserNowInHeadMenuKEY)==menu.name}">
                <span class="selected"></span>
            </g:if><g:else>
                <span class="arrow "></span>
            </g:else>
        </a>
       <g:if test="${menu.childMenus}">
            <ul class="sub-menu">
            <g:each in="${menu.childMenus.sort(true,new Comparator<Menu>() {
                @Override
                int compare(Menu o1, Menu o2) {
                    if(o1.getId()>o2.getId()){
                        return 1
                    }
                    return -1
                }
            })}" var="cm">
                <li <g:if test="${session.getAttribute(SystemConstant.UserNowInChildMenuKEY)==cm.name}">class="active"</g:if>>
                    <a href="${request.getContextPath()}${cm.link}">${cm.name}</a>
                </li>
            </g:each>
            </ul>
        </g:if>
    </li>
</g:each>