<%@ page import="shiyaokeji_erp.Menu" %>
<div class="fieldcontain ${hasErrors(bean: menuInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="menu.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${menuInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: menuInstance, field: 'ico', 'error')} ">
	<label for="ico">
		<g:message code="menu.ico.label" default="Ico" />
		
	</label>
	<g:textField name="ico" value="${menuInstance?.ico}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: menuInstance, field: 'rank', 'error')} required">
	<label for="rank">
		<g:message code="menu.rank.label" default="Rank" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="rank" type="number" value="${menuInstance.rank}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: menuInstance, field: 'parentMenu', 'error')} ">
	<label for="parentMenu">
		<g:message code="menu.parentMenu.label" default="Parent Menu" />
		
	</label>
	<g:field name="parentMenu" type="number" value="${menuInstance.parentMenu}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: menuInstance, field: 'link', 'error')} ">
	<label for="link">
		<g:message code="menu.link.label" default="Link" />
		
	</label>
	<g:textField name="link" value="${menuInstance?.link}"/>
</div>

