package shiyaokeji_erp

class Product {
    String name

    Date dateCreated
    Date lastUpdated

    static constraints = {
        name(nullable: false,blank: false)
    }
}
