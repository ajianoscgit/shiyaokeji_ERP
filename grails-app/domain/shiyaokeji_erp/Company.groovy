package shiyaokeji_erp

/**
 * 购销单位
 */
class Company {

    String name
    String sui_number//纳税人识别号
    String phone
    String address
    String bankAddress//开户行地址
    String bankNumber//银行卡账号
    String description

    Date dateCreated
    Date lastUpdated

    static constraints = {
        name(blank: false,nullable: false)
        sui_number(nullable: true)
        phone(nullable: true)
        bankAddress(nullable: true)
        address(nullable: true)
        description(nullable: true)
        bankNumber(nullable: true)
    }
}

