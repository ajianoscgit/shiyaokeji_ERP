package shiyaokeji_erp

class Menu {
    String name
    String ico
    String link
    Integer rank//菜单等级
    static hasMany = [childMenus:Menu]
    Menu parentMenu//父级菜单

    static mapping = {
        cache(true)
    }
    static constraints = {
        id(nullable: false)
        name(blank:false,nullable: false)
        ico(nullable: true)
        link(nullable: true)
        rank(nullable: true)
        parentMenu(nullable: true)
    }

}
