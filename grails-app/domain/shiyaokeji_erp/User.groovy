package shiyaokeji_erp

class User {
    String name
    String password
    String email
    String phone

    Company company

    static constraints = {
        name(blank:false,nullable: false)
        password(blank: false,nullable: false)
        email(email: true,nullable: true)
        phone(nullable: true)
        company(nullable: true)
    }
}
