package shiyaokeji_erp

class InvoiceType {
    String name
    Date lastUpdated
    Date dateCreated
    static hasMany = [invoices:Invoice]
    static mapping = {

    }
    static constraints = {
        name(nullable: false)
    }
}
