package shiyaokeji_erp
/**
 * 发票
 */
class Invoice {
    String invoiceNumber//发票编号
    InvoiceType invoiceType
    Product product
    String xinghao//规格型号
    String danwei//单位
    Integer count//数量
    Float price
    Float totalMoney//金额
    Float taxRate//税率
    Float tax//税额
    Company buyCompany//购货单位
    Company sellCompany//销货单位

    Date dateCreated
    Date lastUpdated

    static constraints = {
        invoiceNumber(nullable: true)
        xinghao(nullable: true)
        danwei(nullable: true)
    }
}
