package shiyaokeji_erp

import constant.SystemConstant

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserController {

    static allowedMethods = [login:"POST",save: "POST", update: "PUT", delete: "DELETE"]
    def afterInterceptor = {
        session.setAttribute(SystemConstant.UserNowInHeadMenuKEY,'系统设置')
        session.setAttribute(SystemConstant.UserNowInChildMenuKEY,'系统用户')
    }

    def login(User user){
        if(user==null || user.hasErrors()){
            flash.message='用户名或密码验证不合格,请检查!'
            respond user.errors,view:'/login'
        }else{
            List userList = User.findAllByNameAndPassword(user.name,user.password)
            if(!userList||userList.size()!=1){
                flash.message='用户名或密码输入错误,请检查!'
                respond user.errors,view:'/login'
            }else{
                User loginUser = userList.get(0)
                session.setAttribute(SystemConstant.USER_Session_KEY,loginUser)

                //获取跳转路径
                def defaultLink = Menu.findByRank(1,[max:1,sort:'id',order:'asc']).getLink()
                response.status=302
                response.setHeader('location',"${request.contextPath}${defaultLink}")
                response.flushBuffer()

            }
        }

    }

    def loginOut(){
        session.removeAttribute(SystemConstant.USER_Session_KEY)
        flash.message='欢迎使用!'
        redirect url: '/'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

    def show(User userInstance) {
        respond userInstance
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userInstance.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*' { respond userInstance, [status: CREATED] }
        }
    }

    def edit(User userInstance) {
        respond userInstance
    }

    @Transactional
    def update(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userInstance.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
