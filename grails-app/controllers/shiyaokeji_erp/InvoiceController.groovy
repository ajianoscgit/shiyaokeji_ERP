package shiyaokeji_erp




import constant.SystemConstant

import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InvoiceController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def afterInterceptor = {
        session.setAttribute(SystemConstant.UserNowInHeadMenuKEY, '发票管理')
        session.setAttribute(SystemConstant.UserNowInChildMenuKEY, '发票中心')
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Invoice.list(params), model: [invoiceInstanceCount: Invoice.count()]
    }

    //kind=1:查看进项，kind=2:查看销项，m:数据起始月份
    def indexByWhere(Integer max,Integer kind,String m,String e){
        if(kind==null || m==null || (kind!=1&&kind!=2)){
            flash.message='参数传递错误.'
            render view: 'index'
            return
        }
        params.max=Math.min(max?:10,100)

        List list=null
        User user = session.getAttribute(SystemConstant.USER_Session_KEY)
        user=User.findById(user.id)
        int count=0
        Date now = new Date()
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')
        switch (kind){
            case 1:                                                                                                                                                                                                                                                                                                                                                                                                                 
                list=Invoice.findAllByBuyCompanyAndDateCreatedBetween(user.company,(m?sdf.parse(m):now),(e?sdf.parse(e):now),params)
                count=Invoice.countByBuyCompanyAndDateCreatedBetween(user.company,(m?sdf.parse(m):now),(e?sdf.parse(e):now))
                break
            case 2:
                list=Invoice.findAllBySellCompanyAndDateCreatedBetween(user.company,(m?sdf.parse(m):now),(e?sdf.parse(e):now),params)
                count=Invoice.countBySellCompanyAndDateCreatedBetween(user.company,(m?sdf.parse(m):now),(e?sdf.parse(e):now))
                break
        }
        respond list,view: 'indexKind', model: [invoiceInstanceCount: count,kind:kind,m:m,e:e]
    }

    def show(Invoice invoiceInstance) {
        respond invoiceInstance
    }

    def create() {
        respond new Invoice(params)
    }

    @Transactional
    def save(Invoice invoiceInstance) {
        if (invoiceInstance == null) {
            notFound()
            return
        }

        if (invoiceInstance.hasErrors()) {
            respond invoiceInstance.errors, view: 'create'
            return
        }

        invoiceInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'invoiceInstance.label', default: 'Invoice'), invoiceInstance.id])
                redirect invoiceInstance
            }
            '*' { respond invoiceInstance, [status: CREATED] }
        }
    }

    def edit(Invoice invoiceInstance) {
        respond invoiceInstance
    }

    @Transactional
    def update(Invoice invoiceInstance) {
        if (invoiceInstance == null) {
            notFound()
            return
        }

        if (invoiceInstance.hasErrors()) {
            respond invoiceInstance.errors, view: 'edit'
            return
        }

        invoiceInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
                redirect invoiceInstance
            }
            '*' { respond invoiceInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Invoice invoiceInstance) {

        if (invoiceInstance == null) {
            notFound()
            return
        }

        invoiceInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoiceInstance.label', default: 'Invoice'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
