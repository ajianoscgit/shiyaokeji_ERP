import shiyaokeji_erp.Menu

class BootStrap {

    def init = { servletContext ->
        if(Menu.count()==0){
            Menu m1,m2=null
            def menus = [new Menu(ico:'icon-home',link: '/index/',name: '首页',rank:1),
                    m1=new Menu(ico: 'icon-gift',name: '发票管理',rank:1),
                    new Menu(name: '发票中心',link: '/invoice',parentMenu:m1,rank: 2),
                    new Menu(name: '购销单位',link: '/company',parentMenu: m1,rank: 2),
                    new Menu(name: '发票类别',link: '/invoiceType',parentMenu: m1,rank: 2),
                    new Menu(ico:'icon-th',name: '商品管理',link: '/product/',rank: 1),
                    m2=new Menu(ico: 'icon-cogs',name: '系统设置',rank:1),
                    new Menu(name: '系统用户',link: '/user/',parentMenu: m2,rank: 2)
            ]
            menus*.save flush: true
            log.info('init menu successful!')
        }
    }
    def destroy = {
    }
}
