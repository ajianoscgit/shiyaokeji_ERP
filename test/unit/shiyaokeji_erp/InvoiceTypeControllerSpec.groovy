package shiyaokeji_erp



import grails.test.mixin.*
import spock.lang.*

@TestFor(InvoiceTypeController)
@Mock(InvoiceType)
class InvoiceTypeControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.invoiceTypeInstanceList
            model.invoiceTypeInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.invoiceTypeInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def invoiceType = new InvoiceType()
            invoiceType.validate()
            controller.save(invoiceType)

        then:"The create view is rendered again with the correct model"
            model.invoiceTypeInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            invoiceType = new InvoiceType(params)

            controller.save(invoiceType)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/invoiceType/show/1'
            controller.flash.message != null
            InvoiceType.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def invoiceType = new InvoiceType(params)
            controller.show(invoiceType)

        then:"A model is populated containing the domain instance"
            model.invoiceTypeInstance == invoiceType
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def invoiceType = new InvoiceType(params)
            controller.edit(invoiceType)

        then:"A model is populated containing the domain instance"
            model.invoiceTypeInstance == invoiceType
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/invoiceType/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def invoiceType = new InvoiceType()
            invoiceType.validate()
            controller.update(invoiceType)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.invoiceTypeInstance == invoiceType

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            invoiceType = new InvoiceType(params).save(flush: true)
            controller.update(invoiceType)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/invoiceType/show/$invoiceType.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/invoiceType/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def invoiceType = new InvoiceType(params).save(flush: true)

        then:"It exists"
            InvoiceType.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(invoiceType)

        then:"The instance is deleted"
            InvoiceType.count() == 0
            response.redirectedUrl == '/invoiceType/index'
            flash.message != null
    }
}
